__author__ = 'Franco'
from bluetooth import *
import sys
import random


def generateRandom():
    mean = 18
    variation = 5
    return  random.normalvariate(mean,variation)

# search for the temperature app server
uuid = "7e6d5fd3-b9e2-4eac-b562-8679691099fc"
service_matches = find_service( uuid = uuid)
from time import sleep

if sys.version < '3':
    input = raw_input

if len(service_matches) == 0:
    print("couldn't find the temperature app server running")
    sys.exit(0)

first_match = service_matches[0]
port = first_match["port"]
name = first_match["name"]
host = first_match["host"]

print("connecting to \"%s\" on %s" % (name, host))

# Create the client socket
sock=BluetoothSocket( RFCOMM )
sock.connect((host, port))

if len(sys.argv) > 1:
    if sys.argv[1] == "-m":
        print("connected.  type a value for a temperature")
        while True:
            data = input()
            if len(data) == 0: break
            sock.send(data)
else:
    print("connected.  generating temperatures reading")
    while True:
        temp = str(generateRandom())
        print("sending " + temp)
        sock.send(temp)
        sleep(2)
sock.close()


